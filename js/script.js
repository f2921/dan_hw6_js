function createNewUser(){

    let firstname = prompt("Please write firstname");
    let lastname = prompt("Please write lastname");
    let birthday = prompt("dd.mm.yyyy");
    let age = getAge(birthday);

    const human = {
        firstname: '',
        lastname: '',
        birthday: '',
        age: '',
        getLogin: function (){
            console.log(this.firstname.charAt(0).toLowerCase() + this.lastname.toLowerCase())
        },
        getPassword: getPassword(firstname,lastname,birthday)
    };


    let newUser = Object.create(human);

    Object.defineProperty(newUser, "setFirstName", {
        set: function(value) {
            this.firstname = value;
        },
    });

    Object.defineProperty(newUser, "setLastName", {
        set: function(value) {
            this.lastname = value;
        },
    });

    Object.defineProperty(newUser, "setBirthDay", {
        set: function(value) {
            this.birthday = value;
        },
    });

    Object.defineProperty(newUser, "setAge", {
        set: function(value) {
            this.age = value;
        },
    });

    newUser.setFirstName = firstname;
    newUser.setLastName = lastname;
    newUser.setBirthDay = birthday;
    newUser.setAge = age;

    return newUser;

}

function trueFormat(date){

    let changeDate  = date.split('.');//This is change str to array with .
    let trueData = changeDate.reverse().join('-');//This is join array with - ,new Date("2021-01-01");

   return trueData;

}

function getAge(date) {

    let userDate =  trueFormat(date);
    let birthdate = new Date(userDate);
    let cur = new Date();
    let diff = cur-birthdate; // This is the difference in milliseconds

    let ageMath = Math.floor(diff/31557600000);

     return ageMath;
}


function getPassword(firstname,lastname,birthday){

    let mixedString = firstname.charAt(0).toUpperCase() + lastname.toLowerCase() + birthday.slice(-4);

    return mixedString;
}

console.log(createNewUser());

/*1.Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
Чтобы использовать специальный символ как обычный, нужно добавить к нему обратную косую черту: \.. Мы получим точку .

Это называется «экранирование символа».
2. Какие средства объявления функций вы знаете?
    Объявление функции (function definition, или function declaration, или function statement)
состоит из ключевого слова function.

function square(number) {
    return number * number;
}
Функция вида "function declaration statement" по синтаксису является инструкцией (statement),
    ещё функция может быть вида "function definition expression".
    Такая функция может быть анонимной (она не имеет имени). Например, функция square может быть вызвана так:

    var square = function(number) { return number * number; };
var x = square(4); // x получает значение 16

Функции вида "function definition expression" удобны, когда функция передаётся аргументом другой функции.
    Следующий пример показывает функцию map, которая должна получить функцию первым аргументом и массив вторым.

    function map(f, a) {
    var result = [], // Создаём новый массив
        i;
    for (i = 0; i != a.length; i++)
        result[i] = f(a[i]);
    return result;
}
В JavaScript функция может быть объявлена с условием.
    Например, следующая функция будет присвоена переменной myFunc только, если num равно 0:

var myFunc;
if (num === 0) {
    myFunc = function(theObject) {
        theObject.make = 'Toyota';
    }
}
3.Что такое hoisting, как он работает для переменных и функций?
    hoisting учит, что объявление переменной или функции физически перемещается в начало вашего кода,
    хотя в действительности этого не происходит. На самом же деле, объявления переменных и функций
попадают в память в процессе фазы компиляции, но остаются в коде на том месте, где вы их объявили.
    Одним из преимуществ помещения в память объявлений функций до выполнения кода то,
    что можно использовать функцию до её объявления, код работает.
    Hoisting хорошо работает и с другими типами данных и переменными.
    Переменные могут быть инициализированы и использованы до их объявления.
    Однако, они не могут быть использованы без инициализации.
    JavaScript "поднимает" только объявление, но не инициализацию.
    Если вы используете переменную, объявленную и проинициализированную после её использования,
    то значение будет undefined.

 */